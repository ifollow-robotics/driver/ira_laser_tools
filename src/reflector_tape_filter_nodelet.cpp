#include <pluginlib/class_list_macros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_filters/FilterChainNodelet.h>

/**
 * \brief A reflector tape false positives filter
 */

namespace ira_laser_tools
{

class ReflectorTapeFilterNodelet : public sensor_filters::FilterChainNodelet<sensor_msgs::LaserScan>
{
public:
    ReflectorTapeFilterNodelet() : sensor_filters::FilterChainNodelet<sensor_msgs::LaserScan>("filter_chain")
    {}

protected:
    void onInit() override
    {
        sensor_filters::FilterChainNodelet<sensor_msgs::LaserScan>::onInit();
        // Load params
        ros::NodeHandle pnh = getPrivateNodeHandle();
        double min_angle, max_angle;
        pnh.param("detached_point_filter/window_size", dpt_ws_, 1);
        pnh.param("shadow_filter/min_angle", min_angle, 3.);
        pnh.param("shadow_filter/max_angle", max_angle, 177.);
        pnh.param("shadow_filter/angular_window", angular_window_, 1);
        pnh.param("shadow_filter/nb_neighbors", nb_neighbors_, 1);
        min_angle_tan_ = tanf( M_PI * min_angle / 180 );
        max_angle_tan_ = tanf( M_PI * max_angle / 180 );
    }

    bool filter(const sensor_msgs::LaserScan& msgIn, sensor_msgs::LaserScan& msgOut) override
    {
        msgOut = msgIn;
        idx_rmv_.clear();

        // Browse high intentity beams and remove false positives
        int scan_size = msgOut.intensities.size(); // suppose ranges and intensities have the same size
        for (int idx = 0; idx < scan_size; ++idx)
        {
            if (msgOut.intensities[idx] == 1)
            {
                // Deteched point filter: check if there is at least
                // one high-intensity neighbor laser beam
                detachedPointFilter_(idx, msgOut);

                // Shadow filter: laser beam incident angle must be
                // not close to zero (wrt the material surface)
                shadowFilter_(idx, msgOut);
            }
        }

        // Remove false positive reflectors
        for (int k = 0; k < idx_rmv_.size(); ++k)
        {
            msgOut.intensities[idx_rmv_[k]] = 0;
        }
        return true;
    }

private:
    std::vector<int> idx_rmv_;  /// indices to remove high intensity flag
    float min_angle_tan_;       /// shadow filter min incident angle
    float max_angle_tan_;       /// shadow filter max incident angle
    int angular_window_;        /// shadow filter angular window (nb of beams)
    int nb_neighbors_;          /// shadow filter nb neighbors to remove
    int dpt_ws_;                /// deteched point filter window size

    bool isShadow_(const float r1, const float r2, const float r1_r2_angle)
    {
        const float y = r2 * sinf(r1_r2_angle); // opposite leg to r1_r1_angle
        const float x = r1 - r2 * cosf(r1_r2_angle); // adjacent leg to incident angle
        const float tan_ai = fabs(y) / x; // tangent of the incident angle

        if (tan_ai > 0)
        {
            if (tan_ai < min_angle_tan_)
                return true;
        }
        else
        {
            if (tan_ai > max_angle_tan_)
                return true;
        }
        return false;
    }

    void detachedPointFilter_(const int idx, const sensor_msgs::LaserScan& laser_scan)
    {
        // Browse "dpt_ws_" neighbors
        int scan_size = laser_scan.intensities.size();
        for (int k = 1; k < dpt_ws_ + 1; ++k)
        {
            int idx_p, idx_n; // previous and next laser beam idx
            idx - k < 0 ?
                idx_p = (idx - k) + scan_size : idx_p = idx - k;
            idx + k >= scan_size ?
                idx_n = (idx + k) - scan_size : idx_n = idx + k;

            // Should have at least one neighbor reflector
            if (laser_scan.intensities[idx_p] == 0 &&
                laser_scan.intensities[idx_n] == 0)
            {
                idx_rmv_.push_back(idx);
            }
        }
        return;
    }

    void shadowFilter_(const int idx, const sensor_msgs::LaserScan& laser_scan)
    {

        for (int aw_k = -angular_window_; aw_k < angular_window_ + 1; ++aw_k)
        {
            int idx_w = idx + aw_k;
            if (idx_w < 0 || idx_w >= (int)laser_scan.ranges.size() || (int)idx == idx_w)
            {  
                continue; // out of scan bounds or itself
            }

            if (isShadow_(laser_scan.ranges[idx],
                          laser_scan.ranges[idx_w],
                          aw_k * laser_scan.angle_increment))
            {
                for (int irm = std::max<int>(idx - nb_neighbors_, 0);
                    irm <= std::min<int>(idx + nb_neighbors_, (int)laser_scan.ranges.size() - 1);
                    ++irm)
                {
                    // Here we could choose which neighbor to delete:
                    // the closest or the farthest one?
                    // We don't know where the relexive tape is placed,
                    // so we choose to remove both
                    idx_rmv_.push_back(irm);
                }
            }
        }
    }
};

PLUGINLIB_EXPORT_CLASS(ira_laser_tools::ReflectorTapeFilterNodelet, nodelet::Nodelet)

};
