#include "ira_laser_tools/laserscan_multi_merger.h"
#include <dynamic_reconfigure/server.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "laser_multi_merger");
    laserscan_multi_merger::LaserscanMerger _laser_merger;
    ros::spin();
    return 0;
}
