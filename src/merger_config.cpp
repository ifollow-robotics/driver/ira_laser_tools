// -*- mode: c++; coding: utf-8; tab-width: 4 -*-

#include <ira_laser_tools/merger_config.h>
#include <ifollow_utils/misc.h>

namespace laserscan_multi_merger
{

using namespace misc;

MergerConfig::MergerConfig()
{
    merger.angle_increment =  0.0031;
    merger.angle_max = 3.14159;
    merger.angle_min = -3.14159;
    merger.cloud_destination_topic = "cloud_fused";
    merger.destination_frame = "fused_lidar_link";
    merger.intensity_th = 200;
    merger.laserscan_topics = "front/scan back/scan";
    merger.range_max = 50.;
    merger.range_min = 0.38;
    merger.scan_destination_topic = "scan_fused";
    merger.scan_time = 0.0299999993294;
    merger.time_increment = 0.;
}

void MergerConfig::loadRosParamFromNodeHandle(const ros::NodeHandle& nh)
{
    // Merger parameters
    loadParamWithWarn(nh, "angle_increment", merger.angle_increment);
    loadParamWithWarn(nh, "angle_max", merger.angle_max);
    loadParamWithWarn(nh, "angle_min", merger.angle_min);
    loadParamWithWarn(nh, "cloud_destination_topic", merger.cloud_destination_topic);
    loadParamWithWarn(nh, "destination_frame", merger.destination_frame);
    loadParamWithWarn(nh, "intensity_th", merger.intensity_th);
    loadParamWithWarn(nh, "laserscan_topics", merger.laserscan_topics);
    loadParamWithWarn(nh, "range_max", merger.range_max);
    loadParamWithWarn(nh, "range_min", merger.range_min);
    loadParamWithWarn(nh, "scan_destination_topic", merger.scan_destination_topic);
    loadParamWithWarn(nh, "scan_time", merger.scan_time);
    loadParamWithWarn(nh, "time_increment", merger.time_increment);

    default_cfg.reset(new MergerConfig(*this)); // copy the state of the current object
}

void MergerConfig::registerDynamicReconfigurableVars(std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr)
{
    ddy_cfg.reset(new MergerConfig(*this));

    // Merger params exposed to dynamic reconfiguration
    ddr->registerVariable<int>("intensity_th", &ddy_cfg->merger.intensity_th, "reflector intensity threshold", 0, 255);
}

void MergerConfig::loadFromDynamicReconfigure()
{
    merger.intensity_th = ddy_cfg->merger.intensity_th;
}

void MergerConfig::printCfg() const
{
    const std::string name = ros::this_node::getName();

    ROS_INFO_STREAM(name << std::left << std::setw(36) <<
                    ":   destination_frame: " << merger.destination_frame);
    ROS_INFO_STREAM(name << std::left << std::setw(36) <<
                    ":   laserscan_topics: " << merger.laserscan_topics);
    ROS_INFO_STREAM(name << std::left << std::setw(36) <<
                    ":   scan_destination_topic:" << merger.scan_destination_topic);
    ROS_INFO_STREAM(name << std::left << std::setw(36) <<
                    ":   intensity_th:" << merger.intensity_th);
}

}
