#include "ira_laser_tools/laserscan_multi_merger.h"
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/impl/transforms.hpp>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <regex>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(laserscan_multi_merger::LaserscanMergerNodelet, nodelet::Nodelet);

namespace laserscan_multi_merger
{

void LaserscanMerger::laserscanTopicParser_()
{
    // LaserScan topics to subscribe
    ros::master::V_TopicInfo topics;
    ros::master::getTopics(topics);
    // Get node's namespace
    std::string nspace = ros::this_node::getNamespace();
    // remove leading slashes
    nspace = std::regex_replace(nspace, std::regex("^/+"), "");

    std::istringstream iss(cfg_.default_cfg->merger.laserscan_topics);
    std::vector<std::string> tokens;
    std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter<std::vector<std::string> >(tokens));

    std::vector<std::string> tmp_input_topics;


    for (int i = 0; i < tokens.size(); ++i)
    {
        for (int j = 0; j < topics.size(); ++j)
        {
            // it seams that kinetic does not include a leading slash in topics[j].name and melodic does
            std::string topic_name( std::regex_replace(topics[j].name, std::regex("^/+"), "") );
            if ( ((nspace + "/" + tokens[i]).compare(topic_name) == 0) && (topics[j].datatype.compare("sensor_msgs/LaserScan") == 0) )
            {
                tmp_input_topics.push_back(topics[j].name);
                ROS_INFO_STREAM(nname_ << " Found topic: " << topics[j].name);
            }
        }
    }


    sort(tmp_input_topics.begin(), tmp_input_topics.end());
    std::vector<std::string>::iterator last = std::unique(tmp_input_topics.begin(), tmp_input_topics.end());
    tmp_input_topics.erase(last, tmp_input_topics.end());


    // Do not re-subscribe if the topics are the same
    if ( (tmp_input_topics.size() != input_topics_.size()) || !equal(tmp_input_topics.begin(), tmp_input_topics.end(), input_topics_.begin()))
    {

        // Unsubscribe from previous topics
        for (int i = 0; i < scan_subscribers.size(); ++i)
            scan_subscribers[i].shutdown();

        input_topics_ = tmp_input_topics;
        if (input_topics_.size() > 0)
        {
            scan_subscribers.resize(input_topics_.size());
            clouds_modified_.resize(input_topics_.size());
            clouds_.resize(input_topics_.size());
            ROS_INFO_STREAM(nname_ << " Subscribing to topics\t" << scan_subscribers.size());
            for (int i = 0; i < input_topics_.size(); ++i)
            {
                scan_subscribers[i] = nh_.subscribe<sensor_msgs::LaserScan> (input_topics_[i].c_str(), 100, boost::bind(&LaserscanMerger::scanCallback_, this, _1, input_topics_[i]));
                clouds_modified_[i] = false;
                std::cout << input_topics_[i] << " ";
            }
        }
        else
            ROS_INFO_STREAM(nname_ << " Not subscribed to any topic.");
    }
}

LaserscanMerger::LaserscanMerger()
    : tfb_()
    , tfl_(tfb_)
    , nname_(ros::this_node::getName())
{
    // Load parameters
    ros::NodeHandle pnh("~");
    cfg_.loadRosParamFromNodeHandle(pnh);
    ddr_.reset(new ddynamic_reconfigure::DDynamicReconfigure(pnh));
    cfg_.registerDynamicReconfigurableVars(ddr_);
    cfg_.printCfg();
    ddr_->publishServicesTopics();

    // Subscribe to topics
    ROS_INFO_STREAM(nname_ << " Calling laserscanTopicParser_()");
    laserscanTopicParser_();

    // Start publishers
    pub_pcl_ = nh_.advertise<sensor_msgs::PointCloud2> (cfg_.default_cfg->merger.cloud_destination_topic.c_str(), 100, false);
    pub_ls_ = nh_.advertise<sensor_msgs::LaserScan> (cfg_.default_cfg->merger.scan_destination_topic.c_str(), 100, false);
    pub_ls_ibool_ = nh_.advertise<sensor_msgs::LaserScan> (cfg_.default_cfg->merger.scan_destination_topic + "/ibool", 100, false);
}

void LaserscanMerger::scanCallback_(const sensor_msgs::LaserScan::ConstPtr& scan, const std::string& topic)
{

    // Verify that TF knows how to transform from the received scan to the destination scan frame
    sensor_msgs::PointCloud2 cloud_in, cloud_out;
    try
    {
        projector_.projectLaser(*scan, cloud_in, -1.0, laser_geometry::channel_option::Intensity);
        geometry_msgs::TransformStamped transform;
        // TODO: the robot has only 2 fixed lidars, there is no need to look for these transform in every callback
        transform = tfb_.lookupTransform(cfg_.default_cfg->merger.destination_frame, scan->header.frame_id, ros::Time(0));
        pcl_ros::transformPointCloud(cfg_.default_cfg->merger.destination_frame, transform.transform, cloud_in, cloud_out);
    }
    catch (tf2::LookupException& ex)
    {
        ROS_ERROR_STREAM(nname_ << ex.what());
        return;
    }

    // Update clouds member variables
    for (int i = 0; i < input_topics_.size(); ++i)
    {
        if (topic.compare(input_topics_[i]) == 0)
        {
            clouds_[i] = cloud_out;
            clouds_modified_[i] = true;
        }
    }

    // Count how many scans we have
    int totalClouds = 0;
    for (int i = 0; i < clouds_modified_.size(); ++i)
        if (clouds_modified_[i])
            ++totalClouds;

    // Go ahead only if all subscribed scans have arrived
    if (totalClouds == clouds_modified_.size())
    {
        // Start with first cloud
        sensor_msgs::PointCloud2 merged_cloud = clouds_[0];
        clouds_modified_[0] = false;
        // Recursively concatenate
        for (int i = 1; i < clouds_modified_.size(); ++i)
        {
            pcl::concatenatePointCloud(merged_cloud, clouds_[i], merged_cloud);
            clouds_modified_[i] = false;
        }
        // Publish fused point cloud
        pub_pcl_.publish(merged_cloud);
        // Convert it to a laser scan and publish
        pointCloud2LaserScan_(merged_cloud);
    }
}

void LaserscanMerger::pointCloud2LaserScan_(const sensor_msgs::PointCloud2& merged_cloud)
{
    // Populate header and laser scan parameters
    sensor_msgs::LaserScanPtr output = createEmptyScan_(merged_cloud.header); // only ranges
    sensor_msgs::LaserScanPtr output_ib = createEmptyScan_(merged_cloud.header); // boolean intensities

    // Allocate memory for ranges and intensities
    uint32_t ranges_size = std::ceil((output->angle_max - output->angle_min) / output->angle_increment);
    output->ranges.assign(ranges_size, output->range_max + 1.0); // fyi, intensity vec is kept empty
    output_ib->ranges.assign(ranges_size, output->range_max + 1.0);
    output_ib->intensities.assign(ranges_size, 0);
    double range_min_sq = cfg_.default_cfg->merger.range_min * cfg_.default_cfg->merger.range_min;

    // Iterate over the point cloud
    for (sensor_msgs::PointCloud2ConstIterator<float> it(merged_cloud, "x"); it != it.end(); ++it)
    {
        const float &x = it[0];
        const float &y = it[1];
        const float &z = it[2];
        const float &intensity = it[3];

        if (std::isnan(x) || std::isnan(y) || std::isnan(z))
        {
            ROS_DEBUG("rejected for nan in point XYZI(%f, %f, %f)\n", x, y, z);
            continue;
        }

        double range_sq = y * y + x * x;
        if (range_sq < range_min_sq)
        {
            ROS_DEBUG("rejected for range %f below minimum value %f. Point: (%f, %f, %f)", range_sq, range_min_sq, x, y, z);
            continue;
        }

        double angle = atan2(y, x);
        if (angle < output->angle_min || angle > output->angle_max)
        {
            ROS_DEBUG("rejected for angle %f not in range (%f, %f)\n", angle, output->angle_min, output->angle_max);
            continue;
        }
        int index = (angle - output->angle_min) / output->angle_increment;

        // Update scan with range and intensity values
        if (output->ranges[index] * output->ranges[index] > range_sq)
        {
            double range = sqrt(range_sq);
            output->ranges[index] = range;
            output_ib->ranges[index] = range;
            // If laser beam hit a reflector, update intensity outputs
            if (!std::isnan(intensity))
            {
                if (intensity > cfg_.ddy_cfg->merger.intensity_th)
                {
                    output_ib->intensities[index] = 1;
                }
            }
        }
    }

    // Publish scan
    pub_ls_.publish(output);
    pub_ls_ibool_.publish(output_ib);
}

sensor_msgs::LaserScanPtr LaserscanMerger::createEmptyScan_(const std_msgs::Header& header)
{
    sensor_msgs::LaserScanPtr output(new sensor_msgs::LaserScan());
    output->header = header;
    output->header.frame_id = cfg_.default_cfg->merger.destination_frame.c_str();
    output->header.stamp = ros::Time::now();
    output->angle_min = cfg_.default_cfg->merger.angle_min;
    output->angle_max = cfg_.default_cfg->merger.angle_max;
    output->angle_increment = cfg_.default_cfg->merger.angle_increment;
    output->time_increment = cfg_.default_cfg->merger.time_increment;
    output->scan_time = cfg_.default_cfg->merger.scan_time;
    output->range_min = cfg_.default_cfg->merger.range_min;
    output->range_max = cfg_.default_cfg->merger.range_max;
    return output;
}

LaserscanMergerNodelet::LaserscanMergerNodelet(){}
LaserscanMergerNodelet::~LaserscanMergerNodelet(){}

void LaserscanMergerNodelet::onInit()
{
    scan_merger_.reset(new LaserscanMerger());
}

};