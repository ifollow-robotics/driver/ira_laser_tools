#pragma once
#include <ros/ros.h>
#include <string.h>
#include <tf2_ros/transform_listener.h>
#include <laser_geometry/laser_geometry.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <nodelet/nodelet.h>
#include <ddynamic_reconfigure/ddynamic_reconfigure.h>
#include <ira_laser_tools/merger_config.h>

namespace laserscan_multi_merger
{

/**
 * @brief A class that performs multi scan merging
 */

class LaserscanMerger
{
public:
    LaserscanMerger();

private:
    ros::NodeHandle nh_;
    ros::Publisher pub_pcl_;        ///< point cloud
    ros::Publisher pub_ls_;         ///< laser scan
    ros::Publisher pub_ls_ibool_;   ///< laser scan with boolean intensitites
    std::vector<ros::Subscriber> scan_subscribers;
    laser_geometry::LaserProjection projector_;
    tf2_ros::Buffer tfb_;
    tf2_ros::TransformListener tfl_;
    std::vector<bool> clouds_modified_;
    std::vector<sensor_msgs::PointCloud2> clouds_;
    std::vector<std::string> input_topics_;
    std::string nname_;
    MergerConfig cfg_;              ///< cfg object containing class params
    std::shared_ptr <ddynamic_reconfigure::DDynamicReconfigure> ddr_; // handle dynamic reconfigure

    sensor_msgs::LaserScanPtr createEmptyScan_(const std_msgs::Header& header);
    void laserscanTopicParser_();
    void scanCallback_(const sensor_msgs::LaserScan::ConstPtr& scan, const std::string& topic);
    void pointCloud2LaserScan_(const sensor_msgs::PointCloud2& cloud);
};

/**
 * @brief Nodelet-wrapper of the LaserscanMerger class
 */
class LaserscanMergerNodelet : public nodelet::Nodelet
{
public:
    LaserscanMergerNodelet();
    ~LaserscanMergerNodelet();

    /**
     * @brief Initialise the nodelet
     *
     * This function is called, when the nodelet manager loads the nodelet.
     */
    void onInit() override;

private:
    std::unique_ptr<LaserscanMerger> scan_merger_;
};

};