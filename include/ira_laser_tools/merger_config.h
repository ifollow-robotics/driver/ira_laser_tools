
#ifndef MERGER_CONFIG_H
#define MERGER_CONFIG_H

// std
#include <string>
#include <vector>

// ros
#include "ros/ros.h"

// Dynamic reconfigure
#include "ddynamic_reconfigure/ddynamic_reconfigure.h"

// ifollow_utils
#include "ifollow_utils/ros_config.h"

namespace laserscan_multi_merger
{

/**
 * @brief Class to manage the lidar scan merger parameters
 *
 * This class manages the parameters used by the nodelets within this package (see
 * file nodelet_plugins.xml for the nodelet list). It loads the parameters and exposes
 * to dynamic reconfigure.
 * 
 */
class MergerConfig : public ros_cfg::ROSConfig
{
public:

    MergerConfig();

    /**
     * @brief Load parmeters from the ros param server.
     * @param nh const reference to the local ros::NodeHandle
     */
    void loadRosParamFromNodeHandle(const ros::NodeHandle& nh) override;

    /**
     * @brief Register dynamic reconfigurable variables.
     * @param ddr shared pointer to the DDynamicReconfigure object
     */
    void registerDynamicReconfigurableVars(std::shared_ptr<ddynamic_reconfigure::DDynamicReconfigure> ddr) override;

    /**
     * @brief Load current values of the dynamically reconfigurable vars.
     */
    void loadFromDynamicReconfigure() override;

    /**
     * @brief print the configuration
     * 
     */
    void printCfg() const override;

public:
    
    // Generic merger parameters
    struct Merger
    {
        std::string destination_frame;
        std::string cloud_destination_topic;
        std::string scan_destination_topic;
        std::string laserscan_topics;
        double angle_min;
        double angle_max;
        double angle_increment;
        double time_increment;
        double scan_time;
        double range_min;
        double range_max;
        int intensity_th; ///< intensity threshold above which a laser ray is considered to have hit a reflector
    } merger;

    // The reflector tape filter parameters are not included in this class
    // on purpose: these params will not be exposed to dynamic reconfigure

    std::shared_ptr<MergerConfig> ddy_cfg;
    std::shared_ptr<MergerConfig> default_cfg;
};

}

#endif
