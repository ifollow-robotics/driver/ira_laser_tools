#!/bin/bash
rosbag record -e -o ~/.ifollow/lidar_merger /amcl_pose /cmd/cmd_vel_stamped /drive/mobile_base_controller/odom /initialpose /apriltag_localisation/initialpose\
  /lidar/front/downsampled/scan /lidar/back/downsampled/scan /lidar/scan_fused /map /odom /odometry/scan_matcher_back/pose_stamped\
  /odometry/scan_matcher_front/pose_stamped /openimu_decoder/data /openimu_madgwick/data_g_filtered /particlecloud /tag_detections\
  /tf /tf_static /lowLvl/manager/low_lvl_toggling /robots_odometry /localization_score /robot_status /lidar/back/scan /lidar/front/scan\
  /lidar/scan_fused/ibool /lidar/cloud_fused /lidar/back/scan_unfiltered /lidar/front/scan_unfiltered
